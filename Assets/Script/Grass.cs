﻿using UnityEngine;

public class Grass : MonoBehaviour
{
    public Grasses parent;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }


    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            parent.OnPlayerRefuge(this, true);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            parent.OnPlayerRefuge(this, false);
        }
    }

    public void Refuge(bool isRefuge)
    {
        if (isRefuge)
        {
            Color newColor = new Color(1f, 1f, 1f, 0.6f);
            this.gameObject.GetComponent<SpriteRenderer>().color = newColor;
        }
        else
        {
            Color newColor = new Color(1f, 1f, 1f, 1f);
            this.gameObject.GetComponent<SpriteRenderer>().color = newColor;
        }
    }
}
