﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterSkill : MonoBehaviour
{
    public List<Transform> listSkill;

    public Transform skillPanelButtonParent;
    public GameObject selectSkillPanel;
    public List<GameObject> listSelectSkill;

    int currentSkill = 0;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ShowSelectSkill()
    {
        GameObject panel = Instantiate(selectSkillPanel, skillPanelButtonParent);
        listSelectSkill.Add(panel);
        ShowTopSelectSkill();
    }

    public void ShowTopSelectSkill()
    {
        if (listSelectSkill.Count > 0)
        {
            listSelectSkill[0].SetActive(true);
            for (int i = 1; i < listSelectSkill.Count; i++)
            {
                listSelectSkill[i].SetActive(false);
            }
        }
    }

    internal void SelectSkill(SkillType skillType)
    {
        if (listSelectSkill.Count > 0)
        {
            GameObject g = listSelectSkill[0];
            listSelectSkill.RemoveAt(0);
            Destroy(g);
        }
        ShowTopSelectSkill();
        Debug.Log("Select skill type: " + skillType);
        if (currentSkill < 6)
        {
            string path = "Character/Skill/skill_" + (int)skillType;
            Sprite sprite = Resources.Load<Sprite>(path);
            listSkill[currentSkill].gameObject.GetComponent<Image>().sprite = sprite;
            currentSkill++;
        }
        switch (skillType)
        {
            case SkillType.UpAttack:
                this.gameObject.GetComponent<CharaterShootController>().AddDamage(true);
                break;
            case SkillType.UpBlood:
                this.gameObject.GetComponent<CharacterHealth>().UpdateHealthLevel(true);
                break;
            case SkillType.UpMagnet:

                break;
            case SkillType.UpRange:

                break;
            case SkillType.UpShield:
                this.gameObject.GetComponent<CharacterHealth>().AddShield(true);
                break;
            case SkillType.UpSpeed:
                this.gameObject.GetComponent<CharacterController>().AddSpeed(true);
                break;

        }
    }
}
