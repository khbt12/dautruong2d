﻿using System.Collections.Generic;
using UnityEngine;

public enum GrassType
{
    Green = 0, Snow, Blue, Yellow
}

public class Grasses : MonoBehaviour
{
    public GameObject grass;

    [Header("Grass Info")]
    public GrassType grassType;
    public int width = 0;
    public int height = 0;
    private float grassSize = 1f;

    [Header("Children")]
    public List<Grass> children;

    [SerializeField] public List<int> currentPlayerIndex;


    // Start is called before the first frame update
    void Start()
    {
        children = new List<Grass>();
        GenerateGrasses();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void GenerateGrasses()
    {
        foreach (Grass g in children)
        {
            Destroy(g.gameObject);
        }
        children = new List<Grass>();

        string path = "Grass/grass_" + ((int)grassType).ToString();

        Sprite sprite = Resources.Load<Sprite>(path);

        grass.GetComponent<SpriteRenderer>().sprite = sprite;

        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                Vector3 position = this.transform.position + i * grassSize * Vector3.right + j * grassSize * Vector3.up;
                GameObject gr = Instantiate(grass, position, Quaternion.identity, this.transform);
                children.Add(gr.GetComponent<Grass>());
                gr.SetActive(true);
            }
        }
    }

    private int GetIndexOfGrass(Grass grass)
    {
        int index = -1;
        int len = children.Count;
        for (int i = 0; i < len; i++)
        {
            if (grass == children[i])
                return i;
        }

        return index;
    }

    private List<int> GetIndexAround(int index)
    {
        List<int> listIndex = new List<int>();
        listIndex.Add(index);

        int row = index % height;
        int column = index / height;

        if (row == 0)
        {
            listIndex.Add(index + 1);
            if (column == 0)
            {
                listIndex.Add(index + height);
                listIndex.Add(index + height + 1);
            }
            else if (column == width - 1)
            {
                listIndex.Add(index - height);
                listIndex.Add(index - height + 1);
            }
            else
            {
                listIndex.Add(index + height);
                listIndex.Add(index + height + 1);
                listIndex.Add(index - height);
                listIndex.Add(index - height + 1);
            }
        }
        else if (row == height - 1)
        {
            listIndex.Add(index - 1);
            if (column == 0)
            {
                listIndex.Add(index + height);
                listIndex.Add(index + height - 1);
            }
            else if (column == width - 1)
            {
                listIndex.Add(index - height);
                listIndex.Add(index - height - 1);
            }
            else
            {
                listIndex.Add(index + height);
                listIndex.Add(index + height - 1);
                listIndex.Add(index - height);
                listIndex.Add(index - height - 1);
            }
        }
        else
        {
            listIndex.Add(index + 1);
            listIndex.Add(index - 1);
            if (column == 0)
            {
                listIndex.Add(index + height);
                listIndex.Add(index + height + 1);
                listIndex.Add(index + height - 1);
            }
            else if (column == width - 1)
            {
                listIndex.Add(index - height);
                listIndex.Add(index - height + 1);
                listIndex.Add(index - height - 1);
            }
            else
            {
                listIndex.Add(index + height);
                listIndex.Add(index - height);
                listIndex.Add(index + height + 1);
                listIndex.Add(index - height + 1);
                listIndex.Add(index + height - 1);
                listIndex.Add(index - height - 1);
            }
        }

        return listIndex;
    }

    private List<int> GetIndexAround()
    {
        List<int> indexAround = new List<int>();
        foreach (int i in currentPlayerIndex)
        {
            List<int> listIndex = GetIndexAround(i);
            foreach (int j in listIndex)
            {
                if (!indexAround.Contains(j)) indexAround.Add(j);
            }
        }
        return indexAround;
    }

    public void OnPlayerRefuge(Grass grass, bool isRefuge)
    {
        int index = GetIndexOfGrass(grass);
        if (isRefuge)
        {
            if (!currentPlayerIndex.Contains(index))
            {
                currentPlayerIndex.Add(index);
            }
        }
        else
        {
            if (currentPlayerIndex.Contains(index))
            {
                currentPlayerIndex.Remove(index);
            }
        }

        List<int> refugeIndex = GetIndexAround();
        int len = width * height;
        for (int i = 0; i < len; i++)
        {
            children[i].Refuge(refugeIndex.Contains(i));
        }
    }
}
