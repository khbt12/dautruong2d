﻿using UnityEngine;

public class CameraController : MonoBehaviour
{
    CharacterController characterController;
    Vector3 offset;

    // Start is called before the first frame update
    void Start()
    {
        characterController = GameObject.FindObjectOfType<CharacterController>();
        offset = -characterController.gameObject.transform.position + this.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.position = Vector3.Lerp(this.transform.position, characterController.transform.position + offset, 5);
    }
}
