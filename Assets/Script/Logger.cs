﻿using UnityEngine;
using UnityEngine.UI;

public class Logger : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ShowMessage(string message)
    {
        this.gameObject.GetComponent<Text>().text = message;
    }
}
