﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapItem : MonoBehaviour
{
    public GameObject itemPrefab;

    [Header("Map Info")]
    public float width;
    public float height;

    [Header("Item Info")]
    public int numberOfJewel;
    public int numberOfHeart;
    public int numberOfBulletAdd;
    public int numberOfBulletQuantityAdd;

    [Header("Item Parent Transform")]
    public Transform itemTransform;
    public Transform jewelParent;
    public Transform heartParent;
    public Transform bulletAddParent;
    public Transform bulletQuantityAddParent;

    public List<Item> listJewel;
    public List<Item> listHeart;
    public List<Item> listBulletAdd;
    public List<Item> listBulletQuantityAdd;

    public Image itemNotification;
    Text itemNotificationText;


    // Start is called before the first frame update
    void Start()
    {
        listJewel = new List<Item>();
        listHeart = new List<Item>();
        listBulletAdd = new List<Item>();
        listBulletQuantityAdd = new List<Item>();

        GenerateMap();

        itemNotificationText = itemNotification.gameObject.GetComponentInChildren<Text>();

        itemNotification.DOFade(0, 0);
        itemNotificationText.DOFade(0, 0);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void GenerateMap()
    {
        string path = "Character/Item/item_0";
        Sprite sprite = Resources.Load<Sprite>(path);
        for (int i = 0; i < numberOfJewel; i++)
        {
            Vector3 randomPosition = UnityEngine.Random.Range(0, width) * Vector3.right + UnityEngine.Random.Range(0, height) * Vector3.up + itemTransform.position;
            GameObject item = Instantiate(itemPrefab, randomPosition, Quaternion.identity, jewelParent);
            item.GetComponent<SpriteRenderer>().sprite = sprite;
            Item it = item.GetComponent<Item>();
            it.itemType = ItemType.Jewel;
            listJewel.Add(it);
        }

        path = "Character/Item/item_1";
        sprite = Resources.Load<Sprite>(path);
        for (int i = 0; i < numberOfHeart; i++)
        {
            Vector3 randomPosition = UnityEngine.Random.Range(0, width) * Vector3.right + UnityEngine.Random.Range(0, height) * Vector3.up + itemTransform.position;
            GameObject item = Instantiate(itemPrefab, randomPosition, Quaternion.identity, heartParent);
            item.GetComponent<SpriteRenderer>().sprite = sprite;
            Item it = item.GetComponent<Item>();
            it.itemType = ItemType.Heart;
            listHeart.Add(it);
        }

        path = "Character/Item/item_2";
        sprite = Resources.Load<Sprite>(path);
        for (int i = 0; i < numberOfBulletAdd; i++)
        {
            Vector3 randomPosition = UnityEngine.Random.Range(0, width) * Vector3.right + UnityEngine.Random.Range(0, height) * Vector3.up + itemTransform.position;
            GameObject item = Instantiate(itemPrefab, randomPosition, Quaternion.identity, bulletAddParent);
            item.GetComponent<SpriteRenderer>().sprite = sprite;
            Item it = item.GetComponent<Item>();
            it.itemType = ItemType.BulletAdd;
            listBulletAdd.Add(it);
        }

        path = "Character/Item/item_3";
        sprite = Resources.Load<Sprite>(path);
        for (int i = 0; i < numberOfBulletQuantityAdd; i++)
        {
            Vector3 randomPosition = UnityEngine.Random.Range(0, width) * Vector3.right + UnityEngine.Random.Range(0, height) * Vector3.up + itemTransform.position;
            GameObject item = Instantiate(itemPrefab, randomPosition, Quaternion.identity, bulletQuantityAddParent);
            item.GetComponent<SpriteRenderer>().sprite = sprite;
            Item it = item.GetComponent<Item>();
            it.itemType = ItemType.BulletQuantityAdd;
            listBulletQuantityAdd.Add(it);
        }
    }

    internal void ShowNotification(Item item)
    {

        ItemType type = item.itemType;
        string path = "Character/Item/hintPanel_" + ((int)type).ToString();
        Sprite sprite = Resources.Load<Sprite>(path);
        itemNotification.DOFade(255, 0.2f);
        itemNotificationText.DOFade(255, 0.2f);
        switch (type)
        {
            case ItemType.Jewel:
                itemNotification.sprite = sprite;
                itemNotificationText.text = "+" + Item.JEWEL_EXP + " EXP";
                break;
            case ItemType.Heart:
                itemNotification.sprite = sprite;
                itemNotificationText.text = "+" + Item.HEART_HP + " HP";
                break;
            case ItemType.BulletAdd:
                itemNotification.sprite = sprite;
                itemNotificationText.text = "+" + Item.BULLET_ADD + " ĐẠN";
                break;
            case ItemType.BulletQuantityAdd:
                itemNotification.sprite = sprite;
                itemNotificationText.text = "+" + Item.BULLET_QUANTITY_ADD + " TIA";
                break;
        }
        itemNotification.DOFade(0, 1);
        itemNotificationText.DOFade(0, 1f);

    }

    internal void RespawnItem(Item item, float timeToRespawnItem)
    {
        StartCoroutine(RespawnItemEnumerator(item, timeToRespawnItem));
    }

    IEnumerator RespawnItemEnumerator(Item item, float timeToRespawnItem)
    {
        yield return new WaitForSeconds(timeToRespawnItem);
        Vector3 randomPosition = UnityEngine.Random.Range(0, width) * Vector3.right + UnityEngine.Random.Range(0, height) * Vector3.up + itemTransform.position;
        item.gameObject.transform.position = randomPosition;
        item.gameObject.SetActive(true);
    }
}
