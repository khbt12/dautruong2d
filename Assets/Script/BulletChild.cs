﻿using UnityEngine;

public class BulletChild : MonoBehaviour
{
    public Bullet parent;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Stone")
        {
            this.gameObject.SetActive(false);
        }
        else if (collision.gameObject.tag == "Player" || collision.gameObject.tag == "Enemies")
        {
            Debug.Log(parent);
            collision.gameObject.GetComponent<CharacterHealth>().AddHealth(-parent.damage);
            this.gameObject.SetActive(false);
        }
    }
}
