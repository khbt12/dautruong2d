﻿using UnityEngine;

public enum ItemType
{
    Jewel = 0, Heart, BulletAdd, BulletQuantityAdd
}

public class Item : MonoBehaviour
{
    public ItemType itemType;

    public float[] timeToRespawn = new float[4];

    public static int JEWEL_EXP = 10;
    public static int HEART_HP = 15;
    public static int BULLET_ADD = 1;
    public static int BULLET_QUANTITY_ADD = 2;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnUserGetItem(Collider2D collision, bool isPlayer)
    {
        switch (itemType)
        {
            case ItemType.Jewel:
                if (collision.gameObject.GetComponent<CharacterExp>() != null)
                    collision.gameObject.GetComponent<CharacterExp>().AddExp(JEWEL_EXP);
                break;
            case ItemType.Heart:
                collision.gameObject.GetComponent<CharacterHealth>().AddHealth(HEART_HP);
                break;
            case ItemType.BulletAdd:
                if (collision.gameObject.GetComponent<CharaterShootController>() != null)
                    collision.gameObject.GetComponent<CharaterShootController>().AddBulletPoolCount(true);
                break;
            case ItemType.BulletQuantityAdd:
                if (collision.gameObject.GetComponent<CharaterShootController>() != null)
                    collision.gameObject.GetComponent<CharaterShootController>().AddQuantity(true);
                break;
        }
        this.gameObject.SetActive(false);
        MapItem mapItem = GameObject.FindObjectOfType<MapItem>();
        mapItem.RespawnItem(this, timeToRespawn[(int)itemType]);
        if (isPlayer)
        {
            mapItem.ShowNotification(this);
        }
        else
        {

        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            OnUserGetItem(collision, true);
        }
        else if (collision.gameObject.tag == "Enemies")
        {
            OnUserGetItem(collision, false);
        }
        else if (collision.gameObject.tag == "Stone")
        {
            Vector3 offset = this.transform.position - collision.transform.position * 1.5f;
            this.transform.position += offset;
        }
    }


}
