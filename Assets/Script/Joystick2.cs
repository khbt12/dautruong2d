﻿using UnityEngine;
using UnityEngine.EventSystems;

public class Joystick2 : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerUpHandler
{
    [Header("Joystick Image")]
    public Transform Bg;
    public Transform Touch;

    Canvas canvas;
    Vector2 bgSize = Vector2.zero;
    bool touchStart = false;

    private void Start()
    {
        bgSize = (Bg.transform as RectTransform).sizeDelta;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        float r = bgSize.x / 2;
        if (canvas == null) canvas = GetComponentInParent<Canvas>();
        Vector2 anchoredPosition = transform.localPosition;
        anchoredPosition += (canvas.transform as RectTransform).sizeDelta / 2;
        Vector3 newPos = eventData.position / canvas.scaleFactor;
        newPos = newPos - new Vector3(anchoredPosition.x, anchoredPosition.y, 0);
        Touch.localPosition = Vector3.ClampMagnitude(new Vector3(newPos.x, newPos.y, newPos.z), r);
        touchStart = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        Touch.localPosition = Vector3.zero;
        joystickUpFunc();
        touchStart = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        float r = bgSize.x / 2;
        Vector2 anchoredPosition = transform.localPosition;
        anchoredPosition += (canvas.transform as RectTransform).sizeDelta / 2;
        Vector3 newPos = eventData.position / canvas.scaleFactor;
        newPos = newPos - new Vector3(anchoredPosition.x, anchoredPosition.y, 0);
        Touch.localPosition = Vector3.ClampMagnitude(new Vector3(newPos.x, newPos.y, newPos.z), r);
    }

    private void Update()
    {
        if (touchStart)
        {
            joystickDownFunc(Vector3.ClampMagnitude(Touch.localPosition, 1.0f));
        }
    }

    public delegate void JoystickDownFunc(Vector3 direction);
    public JoystickDownFunc joystickDownFunc;

    public delegate void JoystickUpFunc();
    public JoystickUpFunc joystickUpFunc;
}
