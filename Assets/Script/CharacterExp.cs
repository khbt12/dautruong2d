﻿using UnityEngine;
using UnityEngine.UI;

public class CharacterExp : MonoBehaviour
{
    public Slider expSlider;
    Text levelText;

    public int currentLevel = 1;
    private float currentExp;
    private float maxExp;

    // Start is called before the first frame update
    void Start()
    {
        levelText = expSlider.gameObject.GetComponentInChildren<Text>();
        currentExp = 0;
        maxExp = 60;
        expSlider.value = (currentExp / maxExp);
        levelText.text = "LV." + currentLevel;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void AddExp(float expAdd)
    {
        if (expAdd < 0) return;
        else
        {
            currentExp += expAdd;
            if (currentExp >= maxExp)
            {
                expSlider.value = 1;
                currentExp -= maxExp;
                maxExp = (maxExp * 1.5f + 10);
                OnLevelUp();
            }
            expSlider.value = currentExp / maxExp;
        }
    }

    public void OnLevelUp()
    {
        currentLevel++;
        levelText.text = "LV." + currentLevel;

        GetComponent<CharacterSkill>().ShowSelectSkill();
    }
}
