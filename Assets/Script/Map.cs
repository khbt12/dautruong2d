﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Map : MonoBehaviour
{
    public List<Sprite> Grass;
    public List<Sprite> Ground;
    public List<Sprite> Stone;
    public Transform Grounds;
    public Transform Glasses;
    public GameObject groundTemp;
    public GameObject stoneTemp;
    public GameObject grassTemp;
    void Start()
    {
        groundTemp.SetActive(false);

        List<List<int>> maps = new List<List<int>>()
        {
            new List<int>(){ 1,2,2,2,2,2,2,2,2,2,2,3},
            new List<int>(){ 4,5,5,5,5,5,5,5,5,5,5,6},
            new List<int>(){ 4,5,5,511110, 5,5,5,5,5,5,5,6},
            new List<int>(){ 4,5,5,5,5,5,5,5,5,5,5,6},
            new List<int>(){ 4,5,5,5, 521234, 5,5,5,5,5,5,6},
            new List<int>(){ 4,5,5,5,5,5,5,5,5,5,5,6},
            new List<int>(){ 7,8,8,8,8,8,8,8,8,8,8,9},
        };

        Vector2 groundSize = (groundTemp.transform as RectTransform).sizeDelta;
        Vector2 stoneSize = (stoneTemp.transform as RectTransform).sizeDelta;
        Vector2 grassSize = (grassTemp.transform as RectTransform).sizeDelta;


        Vector2 mapSize = new Vector2(groundSize.x * maps[0].Count, groundSize.y * maps.Count);


        for (int i = 0; i < maps.Count; i++)
        {
            List<int> row = maps[i];
            for (int j = 0; j < row.Count; j++)
            {
                GameObject go;
                int indexGround = 0;
                int type = 0;
                List<int> list = new List<int>();
                if (row[j]>=10000)
                {
                    int val = row[j];
                    int num = 100000;
                    indexGround = val / num;
                    int _indexGround;
                    while (val > 0)
                    {
                        _indexGround = val / num;
                        val = val - (_indexGround * num);
                        if (num == 10000)
                        {
                            type = _indexGround;
                        }
                        else if(num<10000)
                        {
                            list.Add(_indexGround);
                        }
                        num = num / 10;
                    }
                  
                }
                else
                {
                    indexGround = row[j];
                }
                Vector2 localPosition = new Vector2(groundSize.x * j + groundSize.x / 2 - mapSize.x / 2, -groundSize.y * i - groundSize.y / 2 + mapSize.y / 2);
                go = Instantiate(groundTemp, Grounds);
                go.GetComponent<Image>().sprite = Ground[indexGround];
                go.SetActive(true);
                go.transform.localPosition = localPosition;

                if (type > 0)
                {
                    Debug.Log("Type:" + type + " Count:" + list.Count);
                    if (type == 1)
                    {
                        int index = 0;
                        for(int m=0;m<=list.Count/2; m++)
                        {
                            for (int k = 0; k<2; k++)
                            {
                                if (m*index<list.Count)
                                {
                                    Debug.Log("goGrass: " + k);
                                    GameObject goGrass = Instantiate(grassTemp, Glasses);
                                    goGrass.GetComponent<Image>().sprite = Grass[list[index] - 1];
                                    goGrass.SetActive(true);
                                    goGrass.transform.localPosition = new Vector2(localPosition.x + k * stoneSize.x - stoneSize.x / 2, localPosition.y + m * stoneSize.y - stoneSize.y / 2);
                                    index++;
                                }
                            }
                        }
                    }
                    else if (type == 2)
                    {
                        int index = 0;
                        for (int m = 0; m <= list.Count / 2; m++)
                        {
                            for (int k = 0; k < 2; k++)
                            {
                                if (m * index < list.Count)
                                {
                                    Debug.Log("goGrass: " + k);
                                    GameObject goGrass = Instantiate(grassTemp, Glasses);
                                    goGrass.GetComponent<Image>().sprite = Stone[list[index] - 1];
                                    goGrass.SetActive(true);
                                    goGrass.transform.localPosition = new Vector2(localPosition.x + k * grassSize.x - grassSize.x / 2, localPosition.y + m * grassSize.y - grassSize.y / 2);
                                    index++;
                                }
                            }
                        }
                    }
                }
               
               

            }
        }

    }
    void Update()
    {

    }
}
