﻿using UnityEngine;
using UnityEngine.UI;

public class SkillButton : MonoBehaviour
{
    public SkillButtonPanel panel;
    public Text skillText;
    SkillType skillType;

    public static Color[] skillTextColor = new Color[6] {
        new Color(164,22,22),
        new Color(82,192,12),
        new Color(164,50,22),
        new Color(255,254,49),
        new Color(61,179,255),
        new Color(13,196,255)
    };

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    internal void LoadSkill(int index)
    {
        skillType = (SkillType)index;
        string path = "Character/Skill/skill_" + index;
        Sprite sprite = Resources.Load<Sprite>(path);
        GetComponent<Image>().sprite = sprite;
        Color color;
        switch (index)
        {
            case 0:
                skillText.text = "+ Sát thương";
                color = new Color(0.64f, 0.09f, 0.09f, 1f);
                skillText.color = color;
                break;
            case 1:
                skillText.text = "+ Máu";
                color = new Color(0.32f, 0.75f, 0.05f, 1f);
                skillText.color = color;
                break;
            case 2:
                skillText.text = "+ Bắn xa";
                color = new Color(0.64f, 0.2f, 0.09f, 1f);
                skillText.color = color;
                break;
            case 3:
                skillText.text = "+ Giáp";
                color = new Color(1f, 1f, 0.19f, 1f);
                skillText.color = color;
                break;
            case 4:
                skillText.text = "+ Tốc độ";
                color = new Color(0.24f, 0.7f, 1f, 1f);
                skillText.color = color;
                break;
            case 5:
                skillText.text = "+ Tầm xa";
                color = new Color(0.05f, 0.77f, 1f, 1f);
                skillText.color = color;
                break;
        }
    }

    public void OnSkillClick()
    {
        panel.SelectSkill(skillType);
    }
}
