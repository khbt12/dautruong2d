﻿using UnityEngine;

public class HPBar : MonoBehaviour
{
    public GameObject player;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        this.transform.position = player.transform.position + Vector3.up * 0.7f;
    }
}
