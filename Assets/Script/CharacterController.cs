﻿using DragonBones;
using UnityEngine;

public class CharacterController : MonoBehaviour
{
    [Header("User Info")]
    public float speed = 2.0f;
    public float jumpSpeed = 8.0f;
    public float gravity = 20.0f;
    float smooth = 5.0f;

    [SerializeField] public bool isJoystick = false;
    //[SerializeField] public bool isAim = false;

    UnityArmatureComponent armatureComponent;

    // Use this for initialization
    void Start()
    {
        armatureComponent = this.gameObject.GetComponent<UnityArmatureComponent>();
    }

    private Vector3 moveDirection = Vector3.zero;

    void Update()
    {
        if (!isJoystick)
        {
            moveDirection = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0.0f);

            Move(moveDirection);
        }
    }

    public void Jump()
    {

    }

    public void Move(Vector3 direction)
    {
        if (direction.x == 0f && direction.y == 0f)
        {
            if (armatureComponent.animation.lastAnimationName != "idle")
            {
                armatureComponent.animation.Stop();
                armatureComponent.animation.Play("idle");
            }
            return;
        }
        //if (!armatureComponent.animation.isPlaying && armatureComponent.animation.lastAnimationName == "idle")
        if (armatureComponent.animation.lastAnimationName == "idle")
        {
            armatureComponent.animation.Stop();
            armatureComponent.animation.Play("walk");
        }
        transform.position += direction.x * Vector3.right * speed * Time.deltaTime;
        transform.position += direction.y * Vector3.up * speed * Time.deltaTime;


        /*
        if (!isAim)
        {
            float angel;

            if (direction.y < 0)
                angel = transform.rotation.y - 90 - Mathf.Atan(direction.x / direction.y) * Mathf.Rad2Deg;
            else
                angel = transform.rotation.y + 90 - Mathf.Atan(direction.x / direction.y) * Mathf.Rad2Deg;

            Quaternion target = Quaternion.Euler(0, 0, angel);
            transform.rotation = Quaternion.Slerp(transform.rotation, target, Time.deltaTime * smooth);
        }
        */
    }

    public void AddSpeed(bool isSpeedUp)
    {
        if (isSpeedUp)
        {
            speed += 0.15f;
        }
        else
        {
            speed = 2.0f;
        }
    }

}
