﻿using UnityEngine;
using UnityEngine.UI;

public class CharacterHealth : MonoBehaviour
{
    [Header("Health Slider")]
    public Slider healthSlider;
    public Image sliderBackground;

    [Header("Health Info")]
    public bool isUnattackable = false;
    public int healthLevel = 5;
    private float healthUnit = 20;
    public float maxHealth = 100;
    public float currentHealth = 80;
    public float shield = 0;

    // Start is called before the first frame update
    void Start()
    {
        healthSlider.value = currentHealth / maxHealth;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void UpdateHealthLevel(bool isLevelUp)
    {
        int lastHeathLevel = healthLevel;
        if (isLevelUp && healthLevel < 8)
        {
            healthLevel++;
        }
        else if (!isLevelUp)
        {
            healthLevel = 5;
        }
        if (lastHeathLevel != healthLevel)
        {
            maxHealth = healthLevel * healthUnit;
            currentHealth += healthUnit;
            Vector3 localScale = healthSlider.transform.localScale;
            healthSlider.transform.localScale = new Vector3(localScale.x * (float)healthLevel / 5, localScale.y, localScale.z);

            string path = "Character/Health/hpFrame_" + healthLevel.ToString();
            Sprite sprite = Resources.Load<Sprite>(path);
            sliderBackground.sprite = sprite;

            healthSlider.value = currentHealth / maxHealth;
        }
    }

    public void AddHealth(float healthAdd)
    {
        if (healthAdd < 0 && isUnattackable) return;
        if (healthAdd > 0) currentHealth += healthAdd;
        else currentHealth += ((healthAdd + shield) < 0) ? (healthAdd + shield) : 0;
        if (currentHealth > maxHealth)
            currentHealth = maxHealth;
        healthSlider.value = currentHealth / maxHealth;
    }

    public void AddShield(bool isShieldUp)
    {
        if (isShieldUp)
        {
            shield += 5;
        }
        else
        {
            shield = 0;
        }
    }
}
