﻿using DragonBones;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Transform = UnityEngine.Transform;

public enum CharacterType
{
    BowMan = 0, Ninja, Sparta, Wizard
}

public class EnemyAI : MonoBehaviour
{
    public CharacterType characterType;

    public float speed = 1.0f;
    float smooth = 5.0f;

    GameObject pool;
    public int bulletQuantity;
    public GameObject[] BulletPrefab;
    Transform bulletPoolParent;
    private List<Bullet> bulletPool;
    UnityArmatureComponent armatureComponent;
    public float damage;
    public bool canShoot = true;

    private Transform target;

    // Start is called before the first frame update
    void Start()
    {
        damage = 15;
        bulletQuantity = 1;
        bulletPoolParent = GameObject.Find("EnemyBulletPool").transform;
        pool = Instantiate(bulletPoolParent.gameObject, bulletPoolParent);
        target = GameObject.FindGameObjectWithTag("Player").transform;
        bulletPool = new List<Bullet>();
        GenerateBulletPool();
        armatureComponent = this.gameObject.GetComponent<UnityArmatureComponent>();
    }

    private void GenerateBulletPool()
    {
        for (int i = 0; i < 5; i++)
        {
            GameObject bullet = Instantiate(BulletPrefab[(int)characterType], this.transform.position, this.transform.rotation, pool.transform);
            Bullet b = bullet.GetComponent<Bullet>();
            b.bulletQuantity = bulletQuantity;
            b.parent = pool.transform;
            b.damage = damage;
            bulletPool.Add(b);
            bullet.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 distance = target.position - this.transform.position;
        pool.transform.position = this.transform.position;
        AutoMove(distance);
        AutoShoot(distance);
    }

    public void AutoMove(Vector3 distance)
    {
        Transform nearestItem = GetNearestItem();
        Vector3 direction = Vector3.ClampMagnitude((nearestItem.position - this.transform.position), 1.0f);
        Debug.Log(direction);
        Move(direction);
    }

    private Transform GetNearestItem()
    {
        List<Item> listItem = GameObject.FindObjectOfType<MapItem>().listJewel;
        int minIndex = 0;
        float minDistance = 0;
        int len = listItem.Count;
        if (len > 0)
            minDistance = (listItem[0].gameObject.transform.position - this.transform.position).magnitude;
        for (int i = 0; i < len; i++)
        {
            float distance = (listItem[i].gameObject.transform.position - this.transform.position).magnitude;
            if (distance < minDistance)
            {
                minIndex = i;
                minDistance = distance;
            }
        }
        return listItem[minIndex].gameObject.transform;
    }

    public void Move(Vector3 direction)
    {
        if (direction.x == 0f && direction.y == 0f)
        {
            if (armatureComponent.animation.lastAnimationName != "idle")
            {
                armatureComponent.animation.Stop();
                armatureComponent.animation.Play("idle");
            }
            return;
        }
        //if (!armatureComponent.animation.isPlaying && armatureComponent.animation.lastAnimationName == "idle")
        if (armatureComponent.animation.lastAnimationName == "idle")
        {
            armatureComponent.animation.Stop();
            armatureComponent.animation.Play("walk");
        }
        transform.position += direction.x * Vector3.right * speed * Time.deltaTime;
        transform.position += direction.y * Vector3.up * speed * Time.deltaTime;

    }


    public void AutoShoot(Vector3 distance)
    {
        if (distance.magnitude < 15)
        {
            Vector3 direction = Vector3.ClampMagnitude(distance, 1.0f);
            Aim(distance);
            if (distance.magnitude < 10)
            {
                if (canShoot)
                {
                    Shoot();
                    canShoot = false;
                    float waitTime = Random.Range(1f, 2f);
                    StartCoroutine(WaitForNextShoot(waitTime));
                }
            }
        }
    }

    IEnumerator WaitForNextShoot(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        canShoot = true;
    }

    public void Aim(Vector3 direction)
    {
        if (direction.x == 0f && direction.y == 0f)
        {
            return;
        }

        float angel;

        if (direction.y < 0)
            angel = transform.rotation.y - 90 - Mathf.Atan(direction.x / direction.y) * Mathf.Rad2Deg;
        else
            angel = transform.rotation.y + 90 - Mathf.Atan(direction.x / direction.y) * Mathf.Rad2Deg;

        Quaternion target = Quaternion.Euler(0, 0, angel);
        transform.rotation = Quaternion.Slerp(transform.rotation, target, Time.deltaTime * smooth);
    }

    public void Shoot()
    {
        foreach (Bullet bullet in bulletPool)
        {
            if (!bullet.gameObject.activeSelf)
            {
                if (armatureComponent.animation.lastAnimationName != "attack")
                {
                    armatureComponent.animation.Stop();
                    armatureComponent.animation.Play("attack");
                }
                bullet.Move(this.transform.rotation);
                return;
            }
        }
    }
}
