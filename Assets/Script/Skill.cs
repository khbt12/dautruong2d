﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SkillType
{
    UpAttack = 0, UpBlood, UpRange, UpShield, UpSpeed, UpMagnet
}

public class Skill : MonoBehaviour
{
    public SkillType skillType;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
