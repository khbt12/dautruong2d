﻿using System.Collections.Generic;
using UnityEngine;

public enum InputType
{
    Automatic, Editor, Mobile
}

public class InputManager : MonoBehaviour
{
    public InputType inputType;

    public List<Joystick> listJoystick;

    //public List<Joystick2> listJoystick2;

    CharacterController characterController;
    CharaterShootController characterShootController;

    // Use this for initialization
    void Start()
    {
        characterController = FindObjectOfType<CharacterController>();
        characterShootController = FindObjectOfType<CharaterShootController>();

        listJoystick[0].joystickDownFunc = (direction) =>
        {
            characterController.isJoystick = true;
            characterController.Move(direction);
        };

        listJoystick[0].joystickUpFunc = () =>
        {
            characterController.isJoystick = false;
        };


        listJoystick[1].joystickDownFunc = (direction) =>
        {
            characterController.isJoystick = true;
            characterShootController.Aim(direction);
        };

        listJoystick[1].joystickUpFunc = () =>
        {
            characterController.isJoystick = false;
            characterShootController.Shoot();
        };


        if (inputType == InputType.Automatic)
        {
#if UNITY_EDITOR || UNITY_STANDALONE
            for (int i = 0; i < listJoystick.Count; i++)
            {
                listJoystick[i].gameObject.SetActive(false);
            }
#elif UNITY_ANDROID || UNITY_IOS
            for(int i = 0; i < listJoystick.Count; i++)
            {
                listJoystick[i].gameObject.SetActive(true);
            }
#endif
        }
        else if (inputType == InputType.Editor)
        {
            for (int i = 0; i < listJoystick.Count; i++)
            {
                listJoystick[i].gameObject.SetActive(false);
            }
        }
        else if (inputType == InputType.Mobile)
        {
            for (int i = 0; i < listJoystick.Count; i++)
            {
                listJoystick[i].gameObject.SetActive(true);
            }
        }
    }


    private void Update()
    {
#if UNITY_ANDROID || UNITY_IOS
        if (listJoystick.Count == 2)
        {
            //GameObject.FindObjectOfType<Logger>().ShowMessage("move fingerId: " + listJoystick[0].fingerId + ", shoot fingerId: " + listJoystick[1].fingerId);

            if (Input.touchCount == 1)
            {
                Touch t = Input.GetTouch(0);
                if (listJoystick[0].fingerId == -1 && listJoystick[1].fingerId == -1)
                {
                    if (listJoystick[0].isTouchJoystickPosition(t.position))
                    {
                        if (listJoystick[0].joystickMoveType == JoystickMoveType.AutoMove)
                        {
                            listJoystick[0].fingerId = 0;
                            if (!listJoystick[0].isMoveToStartPoint)
                            {
                                listJoystick[0].MoveToStartPosition(Input.GetTouch(0).position);
                            }
                        }
                        else if (listJoystick[0].joystickMoveType == JoystickMoveType.NotMoving)
                        {
                            if (CheckTouchObject(t, listJoystick[0].outerCircle.gameObject))
                            {
                                listJoystick[0].fingerId = 0;
                            }
                        }

                    }
                    else if (listJoystick[1].isTouchJoystickPosition(t.position))
                    {
                        if (listJoystick[1].joystickMoveType == JoystickMoveType.AutoMove)
                        {
                            listJoystick[1].fingerId = 0;
                            if (!listJoystick[1].isMoveToStartPoint)
                            {
                                listJoystick[1].MoveToStartPosition(Input.GetTouch(0).position);
                            }
                        }
                        else if (listJoystick[1].joystickMoveType == JoystickMoveType.NotMoving)
                        {

                            if (CheckTouchObject(t, listJoystick[1].outerCircle.gameObject))
                            {
                                listJoystick[1].fingerId = 0;
                            }
                        }
                    }
                }
            }
            else if (Input.touchCount == 2)
            {
                Touch t;
                if (Input.GetTouch(0).fingerId == 1) t = Input.GetTouch(0);
                else t = Input.GetTouch(1);
                if (listJoystick[0].fingerId == -1)
                {
                    if (listJoystick[0].joystickMoveType == JoystickMoveType.NotMoving)
                    {
                        if (CheckTouchObject(t, listJoystick[0].outerCircle.gameObject))
                        {
                            listJoystick[0].fingerId = 1;
                        }
                    }
                    else if (listJoystick[0].joystickMoveType == JoystickMoveType.AutoMove)
                    {
                        listJoystick[0].fingerId = 1;
                    }
                }
                if (listJoystick[1].fingerId == -1)
                {
                    if (listJoystick[1].joystickMoveType == JoystickMoveType.NotMoving)
                    {
                        if (CheckTouchObject(t, listJoystick[1].outerCircle.gameObject))
                        {
                            listJoystick[1].fingerId = 1;
                        }
                    }
                    else if (listJoystick[1].joystickMoveType == JoystickMoveType.AutoMove)
                    {
                        listJoystick[1].fingerId = 1;
                    }
                }
            }
            else if (Input.touchCount == 0)
            {
                listJoystick[0].fingerId = -1;
                listJoystick[0].isMoveToStartPoint = false;
                listJoystick[1].fingerId = -1;
                listJoystick[1].isMoveToStartPoint = false;
            }
        }
        else if (listJoystick.Count == 1)
        {
            if (Input.touchCount == 1)
            {
                Touch t = Input.GetTouch(0);
                if (listJoystick[0].isTouchJoystickPosition(t.position))
                {
                    listJoystick[0].fingerId = 0;
                    if (listJoystick[0].joystickMoveType == JoystickMoveType.AutoMove && !listJoystick[0].isMoveToStartPoint)
                    {
                        listJoystick[0].MoveToStartPosition(t.position);
                    }
                }
            }
            else if (Input.touchCount == 0)
            {
                listJoystick[0].fingerId = -1;
                listJoystick[0].isMoveToStartPoint = false;
            }
        }


#elif UNITY_EDITOR || UNITY_STANDALONE
        Vector2 positionOnScreen = Camera.main.WorldToViewportPoint(characterController.gameObject.transform.position);

        Vector2 mouseOnScreen = (Vector2)Camera.main.ScreenToViewportPoint(Input.mousePosition);

        Vector2 offset = mouseOnScreen - positionOnScreen;

        Vector2 clampMagnitude = Vector3.ClampMagnitude(offset, 1.0f);

        Vector3 direction = new Vector3(clampMagnitude.x, clampMagnitude.y, 0.0f);

        characterShootController.Aim(direction);

        if (Input.GetMouseButtonDown(0))
        {
            characterShootController.Shoot();
        }
#endif

    }


    public bool CheckTouchObject(Touch touch, GameObject gameObject)
    {
        Vector3 touchPosWorld = Camera.main.ScreenToWorldPoint(touch.position);
        Vector2 touchPosWorld2D = new Vector2(touchPosWorld.x, touchPosWorld.y);

        RaycastHit2D hitInformation = Physics2D.Raycast(touchPosWorld2D, Camera.main.transform.forward);

        if (hitInformation.collider != null)
        {
            GameObject touchedObject = hitInformation.transform.gameObject;
            if (GameObject.ReferenceEquals(touchedObject, gameObject))
            {
                return true;
            }
        }

        return false;
    }
}
