﻿using DragonBones;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharaterShootController : MonoBehaviour
{
    float smooth = 5.0f;
    public GameObject aimArea;
    public UnityEngine.Transform bulletPoolTransform;

    [Header("Bullet Prefab")]
    public GameObject[] BulletPrefab;

    [Header("Bullet Info")]
    public BulletType bulletType;
    public int bulletQuantity = 1;
    public float damage = 15;

    [Header("Bullet Info UI")]
    public Text bulletCountText;
    public Text bulletChildCountText;
    public Text damageText;

    private List<Bullet> bulletPool;
    private int bulletPoolCount = 5;

    UnityArmatureComponent armatureComponent;

    // Start is called before the first frame update
    void Start()
    {
        bulletCountText.text = "Đạn: 5/5";
        bulletChildCountText.text = "Số tia: " + bulletQuantity + "/5";
        aimArea.SetActive(false);
        bulletPool = new List<Bullet>();
        GenerateBulletPool();
        armatureComponent = this.gameObject.GetComponent<UnityArmatureComponent>();
    }

    // Update is called once per frame
    void Update()
    {
        bulletPoolTransform.transform.position = this.transform.position;
        UpdateBulletPoolText();
    }

    private void GenerateBulletPool()
    {
        foreach (Bullet b in bulletPool)
        {
            Destroy(b.gameObject);
        }

        bulletPool = new List<Bullet>();

        for (int i = 0; i < bulletPoolCount; i++)
        {
            GameObject bullet = Instantiate(BulletPrefab[(int)bulletType], this.transform.position, this.transform.rotation, bulletPoolTransform);
            Bullet b = bullet.GetComponent<Bullet>();
            b.bulletQuantity = bulletQuantity;
            b.parent = bulletPoolTransform;
            b.damage = damage;
            bulletPool.Add(b);
            bullet.SetActive(false);
        }
    }


    public void Aim(Vector3 direction)
    {
        if (direction.x == 0f && direction.y == 0f)
        {
            return;
        }

        aimArea.SetActive(true);

        float angel;

        if (direction.y < 0)
            angel = transform.rotation.y - 90 - Mathf.Atan(direction.x / direction.y) * Mathf.Rad2Deg;
        else
            angel = transform.rotation.y + 90 - Mathf.Atan(direction.x / direction.y) * Mathf.Rad2Deg;

        Quaternion target = Quaternion.Euler(0, 0, angel);
        transform.rotation = Quaternion.Slerp(transform.rotation, target, Time.deltaTime * smooth);


    }

    public void Shoot()
    {
        aimArea.SetActive(false);
        foreach (Bullet bullet in bulletPool)
        {
            if (!bullet.gameObject.activeSelf)
            {
                if (armatureComponent.animation.lastAnimationName != "attack")
                {
                    armatureComponent.animation.Stop();
                    armatureComponent.animation.Play("attack");
                }
                bullet.Move(this.transform.rotation);
                return;
            }
        }
    }

    public void UpdateBulletQuantity()
    {
        foreach (Bullet bullet in bulletPool)
        {
            bullet.bulletQuantity = this.bulletQuantity;
        }
        this.bulletChildCountText.text = "Số tia: " + bulletQuantity + "/5";
    }

    public void AddQuantity(bool isQuantityUp)
    {
        if (isQuantityUp)
        {
            int lastBulletQuantity = bulletQuantity;
            if (bulletQuantity < 5) bulletQuantity += 2;
            if (lastBulletQuantity == bulletQuantity)
                return;
        }
        else
        {
            bulletQuantity = 1;
        }
        UpdateBulletQuantity();
    }

    public void UpdateBulletPoolText()
    {
        int k = 0;
        foreach (Bullet b in bulletPool)
        {
            if (b.gameObject.activeSelf)
                k++;
        }
        bulletCountText.text = "Đạn: " + (bulletPool.Count - k).ToString() + "/" + bulletPoolCount;
    }

    public void UpdateBulletPool()
    {
        if (bulletPoolCount == bulletPool.Count + 1)
        {
            GameObject bullet = Instantiate(BulletPrefab[(int)bulletType], this.transform.position, this.transform.rotation, bulletPoolTransform);
            Bullet b = bullet.GetComponent<Bullet>();
            b.bulletQuantity = bulletQuantity;
            b.parent = bulletPoolTransform;
            b.damage = damage;
            bulletPool.Add(b);
            bullet.SetActive(false);
        }
    }

    public void AddBulletPoolCount(bool isBulletUp)
    {
        if (isBulletUp)
        {
            int lastBulletPoolCount = bulletPoolCount;
            if (bulletPoolCount < 8) bulletPoolCount++;
            if (lastBulletPoolCount == bulletPoolCount) return;
        }
        else
        {
            bulletPoolCount = 5;
        }
        UpdateBulletPool();
    }

    public void AddDamage(bool isDamageUp)
    {
        if (isDamageUp)
        {
            damage += 7;
        }
        else
        {
            damage = 15;
        }
        foreach (Bullet b in bulletPool)
        {
            b.damage = damage;
        }
    }
}
