﻿using System.Collections.Generic;
using UnityEngine;

public class SkillButtonPanel : MonoBehaviour
{
    public SkillButton[] button;

    // Start is called before the first frame update
    void Start()
    {
        GenerateButton();
    }

    public void GenerateButton()
    {
        List<int> listRandom = new List<int>();
        for (int i = 0; i < 3; i++)
        {
            int number;
            do
            {
                number = UnityEngine.Random.Range(0, 5);
            } while (listRandom.Contains(number));
            listRandom.Add(number);
        }
        for (int i = 0; i < 3; i++)
        {
            button[i].LoadSkill(listRandom[i]);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    internal void SelectSkill(SkillType skillType)
    {
        GameObject.FindObjectOfType<CharacterSkill>().SelectSkill(skillType);
    }
}
