﻿using UnityEngine;

public enum StoneType
{
    Green = 0, Snow, Blue, Yellow
}

public class Stone : MonoBehaviour
{
    public GameObject stone;

    public StoneType stoneType;
    public int width = 0;
    public int height = 0;
    private float stoneSize = 1f;

    // Start is called before the first frame update
    void Start()
    {
        GenerateStone();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void GenerateStone()
    {
        string path = "Stone/stone_" + ((int)stoneType).ToString();

        Sprite sprite = Resources.Load<Sprite>(path);

        stone.GetComponent<SpriteRenderer>().sprite = sprite;

        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                Vector3 position = this.transform.position + i * stoneSize * Vector3.right + j * stoneSize * Vector3.up;
                GameObject st = Instantiate(stone, position, Quaternion.identity, this.transform);
                st.SetActive(true);
            }
        }
    }

}
