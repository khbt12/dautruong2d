﻿using UnityEngine;
using UnityEngine.UI;

public enum JoystickMoveType
{
    NotMoving,
    AutoMove
}

public enum JoystickPositionType
{
    Left, Right,
    LeftDown, RightDown,
    All
}

public class Joystick : MonoBehaviour
{
    public Transform circle;
    public Transform outerCircle;

    public JoystickMoveType joystickMoveType;
    public JoystickPositionType joystickPositionType;

    bool touchStart = false;
    public Vector3 pointA, pointB;
    float joystickSize;
    Vector3 defaultPosition;

    public int fingerId;
    public bool isMoveToStartPoint = false;

    // Use this for initialization
    void Start()
    {
        defaultPosition = Camera.main.WorldToScreenPoint(this.outerCircle.transform.position);
        joystickSize = outerCircle.GetComponent<RectTransform>().rect.width / 2 * 0.8f;
    }

    // Update is called once per frame
    void Update()
    {
        if (joystickMoveType == JoystickMoveType.AutoMove)
        {
#if UNITY_ANDROID || UNITY_IOS
            if (fingerId != -1)
            {
                if (Input.touchCount > 0)
                {
                    Touch touch = Input.GetTouch(0);
                    bool isHaveTouch = false;
                    for (int i = 0; i < Input.touchCount; i++)
                    {
                        if (Input.GetTouch(i).fingerId == fingerId)
                        {
                            touch = Input.GetTouch(i);
                            isHaveTouch = true;
                            break;
                        }
                    }
                    if (isHaveTouch)
                    {
                        if (touch.phase == TouchPhase.Began)
                        {
                            if (isTouchJoystickPosition(touch.position))
                            {
                                MoveToStartPosition(touch.position);
                            }
                        }

                        if (touch.phase == TouchPhase.Moved)
                        {
                            touchStart = true;
                            //pointB = Camera.main.ScreenToWorldPoint(new Vector3(t.position.x, t.position.y, 0));
                        }

                        if (touch.phase == TouchPhase.Ended)
                        {
                            touchStart = false;
                            Blur(true);
                            MoveToDefaultPosition();
                            joystickUpFunc();
                        }
                    }
                }
            }
#endif
        }
        else if (joystickMoveType == JoystickMoveType.NotMoving)
        {
#if UNITY_ANDROID || UNITY_IOS
            if (fingerId != -1)
            {
                if (Input.touchCount > 0)
                {
                    Touch touch = Input.GetTouch(0);
                    bool isHaveTouch = false;
                    for (int i = 0; i < Input.touchCount; i++)
                    {
                        if (Input.GetTouch(i).fingerId == fingerId)
                        {
                            touch = Input.GetTouch(i);
                            isHaveTouch = true;
                            break;
                        }
                    }
                    if (isHaveTouch)
                    {
                        if (touch.phase == TouchPhase.Began)
                        {
                            if (isTouchJoystickPosition(touch.position))
                            {
                                pointA = outerCircle.transform.position;
                                //pointA = Camera.main.ScreenToWorldPoint(touch.position);
                                touchStart = true;
                            }
                        }

                        if (touch.phase == TouchPhase.Moved)
                        {
                            pointA = outerCircle.transform.position;
                            pointB = Camera.main.ScreenToWorldPoint(touch.position);
                            Vector2 offset = pointB - pointA;

                            Vector2 clampMagnitude = Vector3.ClampMagnitude(offset, 1.0f);

                            circle.gameObject.SetActive(true);
                            circle.position = outerCircle.position + new Vector3(clampMagnitude.x, clampMagnitude.y, 0);

                            Vector3 direction = new Vector3(clampMagnitude.x, clampMagnitude.y, 0.0f);

                            joystickDownFunc(direction);
                        }

                        if (touch.phase == TouchPhase.Ended)
                        {
                            touchStart = false;
                            Blur(true);
                            MoveToDefaultPosition();
                            joystickUpFunc();
                            isMoveToStartPoint = false;
                        }
                    }
                }
            }
#endif
        }
    }

    private void FixedUpdate()
    {
        if (touchStart)
        {
            if (fingerId != -1)
            {
                if (Input.touchCount > 0)
                {
                    Touch touch = Input.GetTouch(0);
                    bool isHaveTouch = false;
                    for (int i = 0; i < Input.touchCount; i++)
                    {
                        if (Input.GetTouch(i).fingerId == fingerId)
                        {
                            touch = Input.GetTouch(i);
                            isHaveTouch = true;
                            break;
                        }
                    }
                    if (isHaveTouch)
                    {
                        Blur(false);
                        pointA = outerCircle.transform.position;
                        pointB = Camera.main.ScreenToWorldPoint(new Vector3(touch.position.x, touch.position.y, 0));

                        Vector2 offset = pointB - pointA;

                        Vector2 clampMagnitude = Vector3.ClampMagnitude(offset, 1.0f);

                        circle.gameObject.SetActive(true);
                        circle.position = outerCircle.position + new Vector3(clampMagnitude.x, clampMagnitude.y, 0);

                        Vector3 direction = new Vector3(clampMagnitude.x, clampMagnitude.y, 0.0f);

                        joystickDownFunc(direction);
                    }
                }
            }
        }
    }

    public delegate void JoystickDown(Vector3 direction);
    public JoystickDown joystickDownFunc;

    public delegate void JoystickUp();
    public JoystickUp joystickUpFunc;

    public void MoveToStartPosition(Vector3 pos)
    {
        pointA = Camera.main.ScreenToWorldPoint(new Vector3(pos.x, pos.y, 0));
        outerCircle.gameObject.SetActive(true);
        Vector3 offset = new Vector3(pointA.x - outerCircle.position.x, pointA.y - outerCircle.position.y, 0);
        outerCircle.position += offset;
        circle.position = outerCircle.position;
        isMoveToStartPoint = true;
    }

    public void MoveToDefaultPosition()
    {
        Vector3 p = Camera.main.ScreenToWorldPoint(defaultPosition);
        outerCircle.gameObject.transform.position = p;
        circle.gameObject.transform.position = p;
    }

    public void Blur(bool isBlur)
    {
        Image image = circle.gameObject.GetComponent<Image>();
        var tempColor = image.color;
        if (isBlur)
            tempColor.a = 0.7f;
        else
            tempColor.a = 1f;
        image.color = tempColor;
        image = outerCircle.gameObject.GetComponent<Image>();
        tempColor = image.color;
        if (isBlur)
            tempColor.a = 0.7f;
        else
            tempColor.a = 1f;
        image.color = tempColor;
    }

    public bool isTouchJoystickPosition(Vector3 mousePosition)
    {
        switch (joystickPositionType)
        {
            case JoystickPositionType.Left:
                if (mousePosition.x < Screen.width / 2)
                    return true;
                break;
            case JoystickPositionType.Right:
                if (mousePosition.x > Screen.width / 2)
                    return true;
                break;
            case JoystickPositionType.LeftDown:
                if (mousePosition.x < Screen.width / 2 && mousePosition.y < Screen.height / 2)
                    return true;
                break;
            case JoystickPositionType.RightDown:
                if (mousePosition.x > Screen.width / 2 && mousePosition.y < Screen.height / 2)
                    return true;
                break;
            case JoystickPositionType.All:
                return true;
        }
        return false;
    }
}
