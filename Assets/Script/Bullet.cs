﻿using System.Collections;
using UnityEngine;

public enum BulletType
{
    Arrow = 0, // Cung ten
    Dart,// Phi tieu
    Spear, // Giao
    Wizard // phu thuy
}

public class Bullet : MonoBehaviour
{
    [Header("Bullet Info")]
    public BulletType bulletType;
    public int bulletQuantity;
    public float speed;
    public float timeToLive = 2.0f;
    public Transform parent;
    public float damage = 10;

    [Header("Child")]
    public BulletChild[] children;

    public LayerMask colliderLayer;

    float[] angle;
    bool isCalculateAngel = false;
    bool isMove = false;
    float[] angel = new float[5] { 0, 0, 0, 0, 0 };
    Vector3[] v = new Vector3[5];

    // Start is called before the first frame update
    void Start()
    {
        Init();
    }



    // Update is called once per frame
    void Update()
    {
        if (isMove)
        {
            if (!isCalculateAngel)
            {

                angel[0] = this.transform.eulerAngles.z * Mathf.Deg2Rad;
                for (int i = 1; i < 5; i++)
                {
                    angel[i] = children[i].transform.rotation.eulerAngles.z * Mathf.Deg2Rad;
                }

                v[0] = Mathf.Cos(angel[0]) * Vector3.right + Mathf.Sin(angel[0]) * Vector3.up;
                for (int i = 1; i < 5; i++)
                {
                    v[i] = Mathf.Cos(angel[i]) * Vector3.right + Mathf.Sin(angel[i]) * Vector3.up - v[0];
                }
                isCalculateAngel = true;
            }


            this.transform.position += v[0] * speed * Time.deltaTime;

            for (int i = 1; i < bulletQuantity; i++)
            {
                children[i].transform.position += v[i] * speed * Time.deltaTime;
            }
        }
    }

    public void Init()
    {
        children[0].gameObject.SetActive(true);
        switch (bulletQuantity)
        {
            case 3:
                children[1].gameObject.SetActive(true);
                children[2].gameObject.SetActive(true);
                children[3].gameObject.SetActive(false);
                children[4].gameObject.SetActive(false);
                break;
            case 5:
                children[1].gameObject.SetActive(true);
                children[2].gameObject.SetActive(true);
                children[3].gameObject.SetActive(true);
                children[4].gameObject.SetActive(true);
                break;
            default:
                children[1].gameObject.SetActive(false);
                children[2].gameObject.SetActive(false);
                children[3].gameObject.SetActive(false);
                children[4].gameObject.SetActive(false);
                break;
        }
    }

    public void ReloadPosition()
    {
        isMove = false;
        isCalculateAngel = false;
        this.transform.position = parent.position;
        this.gameObject.SetActive(false);
        foreach (BulletChild child in children)
        {
            child.transform.position = this.transform.position;
            child.gameObject.SetActive(false);
        }
    }

    public void Move(Quaternion rotation)
    {
        this.gameObject.SetActive(true);
        for (int i = 0; i < bulletQuantity; i++)
        {
            children[i].gameObject.SetActive(true);
        }
        isMove = true;
        this.transform.rotation = rotation;
        StartCoroutine(Fly());
    }

    IEnumerator Fly()
    {
        yield return new WaitForSeconds(timeToLive);
        ReloadPosition();
    }
}
